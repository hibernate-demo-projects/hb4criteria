package sh.main;

import java.util.List;

import sh.daos.BookDao;
import sh.entities.Book;
import sh.util.HbUtil;

public class Hb4Main {

	public static void main(String[] args) {
		BookDao bookDao = new BookDao();

		/*
		 * Transaction is compulsory if session object is obtained from
		 * getCurrentSessionM() method.
		 */
		try {
			HbUtil.beginTransaction();

			List<Book> booksByKanetkar = bookDao.findByAuthor("KANETKAR");
			
			List<Book> books = bookDao.findAll();

			HbUtil.commitTransaction();
			
			booksByKanetkar.forEach(System.out::println);
			
			books.forEach(System.out::println);

		} catch (Exception e) {
			HbUtil.rollbackTransaction();
			e.printStackTrace();
		}
	}
}
