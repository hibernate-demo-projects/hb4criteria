package sh.daos;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import sh.entities.Book;
import sh.util.HbUtil;

public class BookDao {
	/*
	 * Create Session object From Session Factory for every new Query via method getCurrentSession().
	 */

	/*
	 * Find Book By Id
	 */
	public Book findBookById(int id) {
		Book book = null;

		// Session object is created by getCurrentSession so there is no need to close it explicitly
		Session session = HbUtil.getCurrentSession();
		return book = session.find(Book.class, id);
	}
	
	/*
	 * Find Book by author name using criteria
	 * 
	 * Criteria is deprecated in hibernate version 4
	 */
	public List<Book> findByAuthor(String authorName) {
		Session session = HbUtil.getCurrentSession();
		Criteria criteria = session.createCriteria(Book.class);
		
		/*
		 * Add Criteria
		 * 
		 * Restriction is helper class which is used to create conditions
		 * 
		 * This is similar to select * from books where author=<authorName>;
		 */
		criteria.add(Restrictions.eq("author",authorName));
		return criteria.list();
	}
	
	/*
	 * Find All Books using criteria
	 */
	public List<Book> findAll(){
		Session session = HbUtil.getCurrentSession();	
		Criteria criteria = session.createCriteria(Book.class);
		/*
		 * If no criteria is given then we will get output like select * from books; (i.e. no where clause)
		 */
		
		/*
		 * To add order by clause
		 */
		criteria.addOrder(Order.desc("price"));
		return criteria.list();
	}
}